#include <cassert>
#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using namespace std;
using namespace chrono;

using write_sequence = vector<string>;

using test_pair = pair<uint64_t, string>;
using modify_sequence = vector<test_pair>;
using read_sequence = vector<test_pair>;

ifstream& operator>>(ifstream& _is, test_pair& _value) {
  _is >> _value.first;
  _is >> _value.second;

  return _is;
}

template <typename S>
S get_sequence(const string& _file_name) {
  ifstream infile(_file_name);
  S result;

  typename S::value_type item;

  while (infile >> item) {
    result.emplace_back(move(item));
  }

  return result;
}

template <typename DataType>
class IndexedBinaryTree {
 public:
  ~IndexedBinaryTree() { RecursiveDelete(root_); }
  void Insert(const DataType& new_element) {
    if (root_ == nullptr) {
      root_ = new Node;
      root_->data = new_element;
      return;
    }

    Node* current_node = root_;
    int i = 0;
    while (current_node != nullptr) {
      ++i;
      if (new_element < current_node->data) {
        ++current_node->left_subtree_size;
        if (current_node->left == nullptr) {
          current_node->left = new Node;
          current_node->left->data = new_element;
          return;
        } else {
          current_node = current_node->left;
        }
      } else {
        ++current_node->right_subtree_size;
        if (current_node->right == nullptr) {
          current_node->right = new Node;
          current_node->right->data = new_element;
          return;
        } else {
          current_node = current_node->right;
        }
      }
    }
  }

  const DataType& Get(int index) {
    Node* current_node = root_;
    while (current_node->left_subtree_size != index) {
      if (current_node->left_subtree_size > index) {
        current_node = current_node->left;
      } else {
        index = index - (current_node->left_subtree_size + 1);
        current_node = current_node->right;
      }
    }
    return current_node->data;
  }

  void Delete(int index) {
    if (index > (root_->left_subtree_size + root_->right_subtree_size) ||
        index < 0)
      throw invalid_argument("Invalid index.\n");

    Node* current_node = root_;
    Node* pred_node = nullptr;

    while (current_node->left_subtree_size != index) {
      if (current_node->left_subtree_size > index) {
        --current_node->left_subtree_size;
        pred_node = current_node;
        current_node = current_node->left;
      } else {
        --current_node->right_subtree_size;
        pred_node = current_node;
        index = index - (current_node->left_subtree_size + 1);
        current_node = current_node->right;
      }
    }

    if (current_node->left == nullptr && current_node->right == nullptr) {
      DeleteNoChildren(current_node, pred_node);
      return;
    }

    if (current_node->left == nullptr || current_node->right == nullptr) {
      DeleteOneChild(current_node, pred_node);
      return;
    }

    Node* swap_node = current_node;
    Node* pred_swap_node = current_node;
    --current_node->left_subtree_size;
    swap_node = swap_node->left;

    while (swap_node->right != nullptr) {
      --swap_node->right_subtree_size;
      pred_swap_node = swap_node;
      swap_node = swap_node->right;
    }
    current_node->data = swap_node->data;
    if (swap_node->left == nullptr && swap_node->right == nullptr) {
      DeleteNoChildren(swap_node, pred_swap_node);
      return;
    }
    DeleteOneChild(swap_node, pred_swap_node);
  }

 private:
  struct Node {
    DataType data;
    Node* left = nullptr;
    Node* right = nullptr;
    int left_subtree_size = 0;
    int right_subtree_size = 0;
  };

 private:
  Node* root_ = nullptr;

  void DeleteNoChildren(Node* current_node, Node* pred_node) {
    if (current_node == root_) {
      delete root_;
      root_ = nullptr;
      return;
    }
    if (pred_node->left == current_node) {
      pred_node->left = nullptr;
    } else if (pred_node->right == current_node) {
      pred_node->right = nullptr;
    } else {
      throw std::runtime_error("!");
    }
    delete current_node;
  }

  void DeleteOneChild(Node* current_node, Node* pred_node) {
    if (current_node == root_) {
      if (root_->left != nullptr) {
        auto left = root_->left;
        delete root_;
        root_ = left;
        return;
      } else if (root_->right != nullptr) {
        auto right = root_->right;
        delete root_;
        root_ = right;
        return;
      } else {
        throw std::runtime_error("!");
      }
    }
    if (pred_node == nullptr) {
      throw std::runtime_error("!");
    }
    if (current_node->left == nullptr && current_node->right == nullptr) {
      throw std::runtime_error("!");
    }
    if (pred_node->left == current_node) {
      if (current_node->left != nullptr) {
        pred_node->left = current_node->left;
      } else {
        pred_node->left = current_node->right;
      }
      delete current_node;
      return;
    } else if (pred_node->right == current_node) {
      if (current_node->left != nullptr) {
        pred_node->right = current_node->left;
      } else {
        pred_node->right = current_node->right;
      }
      delete current_node;
      return;
    } else {
      throw std::runtime_error("!");
    }
  }

  void RecursiveDelete(Node* node) {
    if (node == nullptr) {
      return;
    }
    RecursiveDelete(node->left);
    RecursiveDelete(node->right);
    delete node;
  }
};

class storage {
 public:
  void insert(const string& _str) { data_.Insert(_str); }
  void erase(uint64_t _index) { data_.Delete(_index); }
  const string& get(uint64_t _index) { return data_.Get(_index); }

 private:
  IndexedBinaryTree<string> data_;
};

int main() {
  write_sequence write = get_sequence<write_sequence>("write.txt");
  modify_sequence modify = get_sequence<modify_sequence>("modify.txt");
  read_sequence read = get_sequence<read_sequence>("read.txt");

  storage st;

  for (const string& item : write) {
    st.insert(item);
  }

  uint64_t progress = 0;
  uint64_t percent = modify.size() / 100;

  time_point<system_clock> time;
  nanoseconds total_time(0);

  modify_sequence::const_iterator mitr = modify.begin();
  read_sequence::const_iterator ritr = read.begin();
  for (; mitr != modify.end() && ritr != read.end(); ++mitr, ++ritr) {
    time = system_clock::now();
    st.erase(mitr->first);
    st.insert(mitr->second);
    const string& str = st.get(ritr->first);
    total_time += system_clock::now() - time;

    if (ritr->second != str) {
      cout << "test failed" << endl;
      return 1;
    }

    if (++progress % (5 * percent) == 0) {
      cout << "time: " << duration_cast<milliseconds>(total_time).count()
           << "ms progress: " << progress << " / " << modify.size() << "\n";
    }
  }

  return 0;
}
